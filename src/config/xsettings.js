export default {
  "prod": {
    "localCookieName": "hspc-picker-start-route",
    "sessionStorageName": "hspcAuthorized",
    "sessionCookieName": "JSESSIONID",
    "personaCookieName": "hspc-persona-token",
    "authServerUrl": "https://sandbox-api.logicahealth.org",
    "cookieDomain": "logicahealth.org"
  },
  "dev": {
    "localCookieName": "hspc-picker-start-route",
    "sessionStorageName": "hspcAuthorized",
    "sessionCookieName": "JSESSIONID",
    "personaCookieName": "hspc-persona-token",
    "authServerUrl": "http://localhost:12000",
    "cookieDomain": "localhost"
  },
  "local": {
    "localCookieName": "hspc-picker-start-route",
    "sessionStorageName": "hspcAuthorized",
    "sessionCookieName": "JSESSIONID",
    "personaCookieName": "hspc-persona-token",
    "authServerUrl": "http://localhost:12000",
    "cookieDomain": "localhost"
  },
  "test": {
    "localCookieName": "hspc-picker-start-route",
    "sessionStorageName": "hspcAuthorized",
    "sessionCookieName": "JSESSIONID",
    "personaCookieName": "hspc-persona-token",
    "authServerUrl": "https://sandbox-api-test.logicahealth.org",
    "cookieDomain": "logicahealth.org"
  }
}
